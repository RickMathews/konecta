import * as jwtdecode from 'jwt-decode'

function getTokenExpirationDate (token) {
  const decoded = jwtdecode(token)

  if (decoded.exp === undefined) return null

  const date = new Date(0)
  date.setUTCSeconds(decoded.exp)
  return date
}

function isTokenExpired (token) {
  if (!token) token = localStorage.getItem('token')
  if (!token) return true

  const date = getTokenExpirationDate(token)
  if (date === undefined) return false
  return !(date.valueOf() > new Date().valueOf())
}

function getCustomClaims () {
  const token = localStorage.getItem('token')
  if (!token) return null
  const clainms = jwtdecode(localStorage.getItem('token'))
  return clainms
}
 function isAdmin ()
 {
  const claims = getCustomClaims()
  const band = claims.is_admin
  return band
 }

export { isTokenExpired, getCustomClaims, isAdmin }
