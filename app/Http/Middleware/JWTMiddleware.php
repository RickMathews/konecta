<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JWTMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // JWTAuth::getPayload();
            $auth = JWTAuth::parseToken();

            $user = $auth->authenticate(); // Trato de autenticar
// dd($user);
            $payload = $auth->getPayload(); // Obtengo el payload del JWT
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['status' => 'Token is Invalid']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['status' => 'Token has Expired']);
            }else{
                return response()->json(['status' => 'Authorization Token not found']);
            }
        }

        $request->attributes->add(['user' => $user->toArray()]);
        $request->request->add(['user' => $user->toArray()]);

        return $next($request);
    }
}
