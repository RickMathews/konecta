<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'admin']);
        $permisos = Permission::All();

        $permisosCliente = [];
        foreach ($permisos as $key) {
            $role->givePermissionTo($key['name']);
            $spl = explode('_', $key['name']);
            
            if($spl[1] == 'client') {
                array_push($permisosCliente, $key);
            }
        }

        $role = Role::create(['name' => 'seller']);
        
        foreach ($permisosCliente as $key) {
            $role->givePermissionTo($key['name']);
        }

        $role = Role::create(['name' => 'client']);
    }
}
